Evolución poblacional de los virus

Angie Geraldine Fandiño Benavides
Fundación universitaria Konrad Lorenz
Docente: Alejandro Cardenas
Facultad de matemáticas e ingenierias

Este  proyecto  tiene  como  propósito  evaluar  tendencias  evolutivas  de  los  virus,  mediante  el
método de Monte Carlo, y el método de cadenas de Markov, con ayuda de la herramienta del
lenguaje de programación Python.

Planteamiento del problema
Así como el ser humano ha evolucionado a lo largo de la
historia, este mismo proceso se ha venido dando con el resto
de seres vivos que habitan nuestro plantea, tales como: virus,animales, 
entre otros. Una de las preguntas que pueden surgir
es: ¿evolucionan con la misma velocidad?.Objetivo general
Analizar  el  comportamiento  y  estructura  de  diferentes
virus en el cuerpo humano, mediante métodos matemáticos,los  cuales,  
generan  muestras  aleatorias  en  un  tiempo  con-tinuo.  Con el fin 
de simular matemáticamente las variantesen las poblaciones virales.

Objetivos específicos
•Implementar modelos evolutivos de los virus usando
el lenguaje de programación Python.
•Realizar  una  comparación  del  comportamiento  dediferentes virus, 
tales como: H1N1, entre otros.
•Analizar  los  resultados  obtenidos  en  la  evolución
poblacional de diferentes virus.

MCMC
El método MCMC no solo tiene una gran utilidad en
el ámbito matemático, por ser un método estocástico permite  que 
se  pueda  aplicar  a  diferentes  áreas  deconocimiento, 
como la biología.

La  idea  deMC MCconsiste  en,  
estimar  el  valoresperado de f por

E(f)=∑x1,...,xnf(x1,...,xn)P(x1,...,xn)≈1\T∑Tx1,...,xnf(x1,...,xn)

Donde:
–P(x1,...,xn)=Distribución de probabilidad.
–xi=Variables del modelo


References
[1]  Garcıa,  L.  O.  B.  Persistencia,  extinción  e  invasión  de  unaepidemia: Retroalimentación de un modelo matemático.
[2] Chavez Ramirez,  F. Y. (2015). Extensión al proceso de simu-lación de poblaciones virales en secuencias genéticas, usando laprogramación paralela CUDA (Doctoral dissertation, Universi-dad Central “Marta Abreu” de Las Villas).